package com.atlassian.marshalling.api;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class MarshallingPairTest {
    @Test
    public void itWorks() {
        final Marshaller<String> m = obj -> new byte[0];
        final Unmarshaller<String> u = raw -> "";

        final MarshallingPair<String> pair = new MarshallingPair<>(m, u);

        assertThat(pair.getMarshaller(), is(m));
        assertThat(pair.getUnmarshaller(), is(u));
    }
}
