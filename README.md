# Server/DC Only

At present this repository is only used by Server/DC products: Jira and Confluence
via `com.atlassian.vcache:atlassian-vcache-api` dependency.
This code has been monorepoed in Jira Cloud, and will be monorepoed if Confluence Cloud needs to change. 

---

Welcome to the home of the Atlassian Marshaller API.

## Issues
Raise issues in [MARSHAL](https://ecosystem.atlassian.net/projects/MARSHAL/summary).

## Versioning
This library is using the [Semantic Versioning 2.0.0](http://semver.org/spec/v2.0.0.html) approach for version numbers.

## Releases
Use `mvn release:prepare` and `mvn release:perform`.

## Building
The prerequisites for building are:

- Java 8 - if you're using [jEnv](http://www.jenv.be/) then the required `.java-version` file exists.
- Maven 3.2.5 - if you're using [Maven Version Manager](http://mvnvm.org/) then the required `mvmvm.properties` file exists.

To build, use the following:

    mvn clean verify

Recommend using `mvn javadoc:javadoc` to get the Javadoc links verified.

CI builds: [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/AMA)

## Code Conventions
Follow the [Platform Rules Of Engagement](http://go.atlassian.com/proe) for committing to this project.

## Checkstyle
Checkstyle is used to enforce coding standards. It is recommended that
you use the supplied Git
[resources/git-hooks/pre-commit](resources/git-hooks/pre-commit) hook to
verify the files before committing.

To install:

    prompt$ cd <path-to-repo>
    prompt$ ln -s ../../resources/git-hooks/pre-commit .git/hooks/pre-commit
    prompt$ chmod a+x .git/hooks/pre-commit
